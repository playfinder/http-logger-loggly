<?php

namespace MLP\HttpLoggerLoggly\Monolog;

class HttpDataProcessor
{
    public function __invoke($record)
    {
        $http_data         =  $record['http'];
        $record['message'] = sprintf("\"%s %s HTTP/%s\" %s", $http_data['method'], $http_data['target'], $http_data['version'], $http_data['code']);

        /**
         * Add some extra metadata if this is a request to post slots
         */
        $matches = [];
        preg_match('/\"POST \/pitches\/(\d+)\/slots(\?[a-zA-Z]*=[a-zA-Z]*)? HTTP\/1.1\" 201/', $record['message'], $matches);

        if (isset($matches[1])) {

            $slots = json_decode($record['http']['req_body'], true);
            $slots = $slots['data'];

            $record['extra'] = [
                'action'    => 'pull-availabilities',
                'pitch_id'  => $matches[1],
                'num_slots' => count($slots)
            ];
        }

        /**
         * Add some extra metadata if this is a request to update a booking
         */
        $matches = [];
        preg_match("/\"PUT \/bookings\/(\d+) HTTP\/1.1\" 200/", $record['message'], $matches);

        if (isset($matches[1])) {

            $booking = json_decode($record['http']['req_body'], true);

            $record['extra'] = [
                'action'   => 'book',
                'pitch_id' => $matches[1],
                'status'   => $booking['data']['attributes']['status']
            ];
        }

        if ($record['http']['method'] == 'GET') {
            unset($record['http']['req_body']);
            unset($record['http']['res_body']);
            unset($record['http']['res_headers']);
            unset($record['http']['req_headers']);
        }

        /**
         * Remove req_body, res_body, req_headers and res_headers for succesful requests to MyLocalPitch
         */
        if (substr($record['http']['host'], 0, 6) == "api-v2" && intval($record['http']['code']) < 299){
            unset($record['http']['req_body']);
            unset($record['http']['res_body']);
            unset($record['http']['res_headers']);
            unset($record['http']['req_headers']);
        }

        return $record;
    }
}
