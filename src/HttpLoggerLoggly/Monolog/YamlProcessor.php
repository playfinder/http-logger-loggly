<?php

namespace MLP\HttpLoggerLoggly\Monolog;

use Symfony\Component\Yaml\Yaml;

class YamlProcessor extends HttpDataProcessor
{
    public function __invoke($record)
    {
        $http_data         = Yaml::parse($record['message']);
        $record['http']    = $http_data;

        return parent::__invoke($record);
    }
}