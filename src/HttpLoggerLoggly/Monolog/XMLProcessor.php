<?php

namespace MLP\HttpLoggerLoggly\Monolog;

use Symfony\Component\Yaml\Yaml;

class XMLProcessor extends HttpDataProcessor
{
    public function __invoke($record)
    {
        $http_data      = $this->simpleXmlToArray(new \SimpleXMLElement($record['message']));
        $record['http'] = $http_data;

        return parent::__invoke($record);
    }

    function simpleXmlToArray($xmlObject)
    {
        $array = [];
        foreach ($xmlObject->children() as $node) {
            $array[$node->getName()] = is_array($node) ? $this->simpleXmlToArray($node) : (string) $node;
        }

        return $array;
    }

}