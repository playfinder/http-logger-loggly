<?php

namespace MLP\HttpLoggerLoggly\Guzzle;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;

class LoggingClient extends Client
{
    public function __construct(array $config)
    {
        if (isset($config['logger'])) {

            $handlerStack = HandlerStack::create(new CurlHandler());
            $handlerStack->push(Middleware::log($config['logger'], new MessageFormatter(MessageFormat::XML_FORMAT)));

            $config['handler'] = $handlerStack;

            unset($config['logger']);
        }

        return parent::__construct($config);
    }
}