<?php

namespace MLP\HttpLoggerLoggly\Guzzle;


class MessageFormat
{
    const YAML_FORMAT = <<<EOF
---
host: {host}
method: {method}
uri: {uri}
target: {target}
hostname: {hostname}
phrase: {phrase}
version: {version}
error: {error}
code: {code}
res_body: '{res_body}'
req_body: '{req_body}'
req_headers: '{req_headers}'
res_headers: '{res_headers}'
EOF;

    const XML_FORMAT = <<<XML
<?xml version='1.0' standalone='yes'?>
<http>
    <host><![CDATA[{host}]]></host>
    <method><![CDATA[{method}]]></method>
    <res_body><![CDATA[{res_body}]]></res_body>
    <uri><![CDATA[{uri}]]></uri>
    <target><![CDATA[{target}]]></target>
    <hostname><![CDATA[{hostname}]]></hostname>
    <phrase><![CDATA[{phrase}]]></phrase>
    <version><![CDATA[{version}]]></version>
    <error><![CDATA[{error}]]></error>
    <code><![CDATA[{code}]]></code>
    <res_body><![CDATA[{res_body}]]></res_body>
    <req_body><![CDATA[{req_body}]]></req_body>
    <res_headers><![CDATA[{res_headers}]]></res_headers>
    <req_headers><![CDATA[{req_headers}]]></req_headers>
</http>
XML;

}
